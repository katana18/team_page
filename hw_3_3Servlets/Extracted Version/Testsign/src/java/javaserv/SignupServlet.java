/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaserv;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Emil Rahimov
 */
public class SignupServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        HttpSession session = request.getSession();
        String error_msg = "";
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String re_password = request.getParameter("re_password");
        
        if(session.getAttribute("authorized") == null){
            session.setAttribute("authorized", "no");
        }
        
        if(username != null && !username.equals("") && password != null && !password.equals("")){
            if(password.equals(re_password)){
                session.setAttribute("username", username);
                session.setAttribute("password", password);  //now signed up
                response.sendRedirect("/Testsign/LoginServlet");
            } else {
                //please password and retype not matching error
                error_msg = "password and retyped password not matching";
            }
        } else {
            // username cannot be empty
            error_msg = "username or password cannot be empty.";
        }
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SignupServlet</title>");   
            out.println("<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>");
            out.println("<link rel='stylesheet' href='design_serv.css' type='text/css' />");            
            out.println("</head>");
            out.println("<body>");
            if(session.getAttribute("authorized").equals("no")){
                out.println("<div id='holderdiv'>");
                out.println("<h1>Welcome!</h1>");
                out.println("<h3 style='color:red;'>" + error_msg + "</h3>");
                out.println("<form action='/Testsign/SignupServlet' method='POST' >");
                out.println("Username: <input type='text' name='username' autofocus required/>");
                out.println("Password: <input type='password' name='password' required />");
                out.println("Re-Type Password: <input type='password' name='re_password' required/>");
                out.println("<input type='submit'  />");
                out.println("</form>");
                out.println("<p><a href='/Testsign/'>Go Home</a></p>");
                out.println("<p><a href='/Testsign/LoginServlet'>Go Hard and Log In</a></p>");
                out.println("</div>");
            } else {
                out.println("<div id='holderdiv'>");
                out.println("<h1>Good to see you again..</h1>");
                out.println("<p>You already Signed Up.</p>");
                out.println("<p><a href='/Testsign/'>Go Home</a></p>");
                out.println("</div>");
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
