/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaserv;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Malik Novruzov
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();
        String error_msg = "Please authorise.";
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String userAdmin = this.getServletConfig().getInitParameter("userAdmin");
        String passAdmin = this.getServletConfig().getInitParameter("passAdmin");
        
        if(session.getAttribute("authorized") == null){
            session.setAttribute("authorized", "no");
        }
        if(userAdmin.equals(username) && passAdmin.equals(password)){ //check for administrator
            session.setAttribute("authorized", "yes"); //OK, admin logged in
            response.sendRedirect("/Testsign");
        }
        
        if(session.getAttribute("username")!=null && session.getAttribute("username").equals(username)  ){ //check for users
            if(session.getAttribute("password")!= null && session.getAttribute("password").equals(password) ){
                session.setAttribute("authorized", "yes"); //OK, so logged in
                response.sendRedirect("/Testsign");
            } else {
                //password is wrong
                error_msg = "Invalid Password";
            }
        } else {
            //username is wrong
            error_msg = "Invalid Username";
        }
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");   
            out.println("<link href='https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>");
            out.println("<link rel='stylesheet' href='design_serv.css' type='text/css' />");
            out.println("</head>");
            out.println("<body>");
            if(session.getAttribute("authorized").equals("no")){
                out.println("<div id='holderdiv'>");
                out.println("<h1>Welcome!</h1>");
                out.println("<h3 style='color:red;'>" + error_msg + "</h3>");
                out.println("<p>Please type your credentials </p>");
                out.println("<form action='/Testsign/LoginServlet' method='POST' >");
                out.println("Username: <input type='text' name='username' autofocus required/>");
                out.println("Password: <input type='password' name='password' required/>");
                out.println("<input type='submit'  />");
                out.println("</form>");
                out.println("<p><a href='/Testsign/'>Go Home</a></p>");
                out.println("<p><a href='/Testsign/SignupServlet'>Go Hard and Sign Up</a></p>");
                out.println("</div>");
            } else{
                out.println("<div id='holderdiv'>");
                out.println("<h1>Good to see you again..</h1>");
                out.println("<p>You already logged In.</p>");
                out.println("<p><a href='/Testsign/'>Go Home</a></p>");
                out.println("</div>");
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
