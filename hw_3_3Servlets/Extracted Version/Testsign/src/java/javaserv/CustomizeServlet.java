/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaserv;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tuncay Habibbayli
 */
public class CustomizeServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession();
        String bgColor = request.getParameter("bgColor");
        String textColor = request.getParameter("textColor");
        
        if(session.getAttribute("authorized") == null){
            session.setAttribute("authorized", "no");
        }
        if(session.getAttribute("authorized").equals("no")){
            response.sendRedirect("/Testsign/LoginServlet");
        }
        
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            
            Map cookieMap = new HashMap();
            Cookie[] cookies = request.getCookies();
            Cookie c1 = new Cookie("bgColor", bgColor);
            Cookie c2 = new Cookie("textColor", textColor);
            if(cookies!=null){
                if(cookies.length == 1){//if there is only sessionid in cookie, then add new cookies
                    response.addCookie(c1);
                    response.addCookie(c2);
                } else {//in case already exists
                    //cookies[0] is JsessionID
                    cookies[1].setValue(bgColor);
                    cookies[2].setValue(textColor);
                }
                for(int i = 0; i < cookies.length; i++){
                        cookieMap.put(cookies[i].getName(), cookies[i].getValue()); //put all cookies into map, this way easy to get values
                }
            } else {
                //cookieMap.put(c1.getName(), c1.getValue()); 
                //cookieMap.put(c2.getName(), c2.getValue());
            }
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CustomizeServlet</title>");
            out.println("<link href=\"https://fonts.googleapis.com/css?family=Oswald:400,700\" rel=\"stylesheet\" type=\"text/css\">");
            out.println("<link rel=\"stylesheet\" href=\"design_serv.css\" type=\"text/css\" />");
            out.println("</head>");
            out.println("<body id=\"body\">");
            out.println("<div id='holderdiv'>");
            out.println("<h1>Hey buddy, Just give some costumization to your page. </h1>");
            
            out.println("<form action=\"/Testsign/CustomizeServlet\" method=\"POST\">");
            out.println("Background Color: <input type=\"color\" value='#FFFFFF' name=\"bgColor\" />");
            out.println("Text Color: <input type=\"color\" value='#000000' name=\"textColor\"/>");
            out.println("<input type=\"submit\" />");
            out.println("</form>");
            out.println("<p><a href=\"/Testsign/\">Go Home</a></p>");
            out.println("</div>");
            
            out.println("<script>");
            out.println("var body = document.getElementById(\"body\");");
            out.println("body.style.backgroundImage = 'none';"); //otherwise, bgcolor not working
            out.println("body.style.backgroundColor = \"" + cookieMap.get("bgColor") + "\";" );
            out.println("body.style.color = \"" + cookieMap.get("textColor") +"\";" );
            out.println("</script>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
